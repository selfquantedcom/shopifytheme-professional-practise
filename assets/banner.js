
$(document).ready(function() {  
    var banner = document.getElementById('contact-banner');    
       
    var closeBtn = banner.querySelector('.slick-close');
    var submitBtn = banner.querySelector('.slick-form-submit');
    var collapsedBanner = banner.querySelector('.slick-collapsed-banner');
    var textArea = banner.querySelector('.slick-feedback-form');      
 
    collapsedBanner.onclick = function (e) {
       hideBanner('slick-collapsed-banner'); 
       showBanner('slick-opened-banner'); 
       hideUserWarning();             
    };
 
    closeBtn.onclick = function (e) {
       collapseBanner();
    };      
 
    submitBtn.onclick = function(e){
       e.preventDefault();               
       let textAreaValue = getTextAreaValue();
       if (textAreaValue) {
          document.querySelector(".slick-banner-body").style.visibility = "hidden";
          document.querySelector(".slick-banner-heading").style.display = "none";
          document.querySelector(".slick-thank-you-message").style.display = "block";         
          setTimeout(removeBanner, 10000);         
       } else {
          displayUserWarning();
       }         
    };
 
    textArea.onclick = function (e) {
       document.getElementById("slick-feedback-form").placeholder = "";      
    };         
 
    collapseBannerOnOutsideClick();    
    restorePlaceholderText();              
 });
 
 // Helper Functions 
 function showBanner(className){
    document.querySelector(`.${className}`).style.display = "block";
 };
 
 function hideBanner(className){
    document.querySelector(`.${className}`).style.display = "none";
 };
 
 function collapseBannerOnOutsideClick(){
    document.addEventListener("click", function(event){
       if (!event.target.closest("#contact-banner")){
          if(document.querySelector(".slick-thank-you-message").style.display == "block"){
             removeBanner();
          } else{
             hideBanner("slick-opened-banner");
             showBanner("slick-collapsed-banner");
          }            
       }              
    });      
 }
 
 function restorePlaceholderText(){
    document.addEventListener("click", function(event) {
       if (!event.target.closest(".slick-feedback-form")){
          document.getElementById('slick-feedback-form').placeholder = "Your message...";
       }      
    });
 }
 
 function getTextAreaValue(){
    return document.getElementById("slick-feedback-form").value
 }
 
 function displayUserWarning(){
    document.getElementById("user-warning").style.display = "block";
 }
 
 function hideUserWarning(){
    document.getElementById("user-warning").style.display = "none"; 
 }
 
 function collapseBanner(){
    hideBanner('slick-opened-banner');
    showBanner('slick-collapsed-banner');      
 }
 
 function removeBanner(){
    var banner = document.getElementById("contact-banner");
    document.body.removeChild(banner);  
 }
    