// Put your applicaiton javascript here
// Full page scroll - only deploy on large screen where content fits to screen

// var currentHash = '';

// function scrollToHash(hash) {
//     $('html, body').animate(
//         {
//             scrollTop: $('#' + hash).offset().top - 50,
//         },
//         800
//     );
// }

// var wait = false;

// function myScroll() { 

//     if (wait) {
//         window.scrollTo(0, 969);
//     }
//     wait = true;
//     setTimeout(function(){ wait = false}, 700);

//     // First section on top will not have hash in the URL and only scroll down is possible
//     if (currentHash) {
//         if (currentHash == 'shopify-section-business_description') {
//             // true on scroll up
//             if(this.oldScroll > this.scrollY) {
//                 currentHash = '';
//                 scrollToHash('shopify-section-hero_image');
//             } else {
//                 var newHash = 'shopify-section-map';
//                 scrollToHash(newHash);
//                 currentHash = newHash;
//             }
//         } else if (currentHash == 'shopify-section-map') {
//             if(this.oldScroll > this.scrollY) {
//                 var newHash = 'shopify-section-business_description';
//                 scrollToHash(newHash);
//                 currentHash = newHash;
//             }
//         }

//     // Handle scroll down from first section on top
//     } else {         
//         // true on scroll down 
//         if(this.oldScroll < this.scrollY) {
//             var newHash = 'shopify-section-business_description';
//             scrollToHash(newHash);
//             currentHash = newHash;
//         }
//     }

//     // Store current scroll coordinates
//     this.oldScroll = this.scrollY; 
// }

// window.onscroll = function () { 
//     myScroll();    
// };

// var element = document.getElementById('shopify-section-business_description');
// var rect = element.getBoundingClientRect();
// console.log(rect.top, rect.right, rect.bottom, rect.left);